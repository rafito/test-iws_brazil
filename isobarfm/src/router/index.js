import Vue from 'vue'
import Router from 'vue-router'
import Bands from '@/components/Bands'
import Band from '@/components/Band'
import Album from '@/components/Album'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      component: Bands
    },
    {
      path: '/band/:id',
      component: Band
    },
    {
      path: '/album/:id',
      component: Album
    }
  ]
})
